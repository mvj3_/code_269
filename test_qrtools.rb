###
 # Encode a QR code
 require 'qrtools'
 require 'tempfile'

 filename = File.join(Dir::tmpdir, 'test.png')
 
 File.open(filename, 'wb') { |fh|
   fh.write QRTools::QRCode.encode('http://www.eoe.cn/').to_png
 }

 ###
 # Decode A QR code from a file
 img = QRTools::Image.load(filename)
 decoder = QRTools::QRCode.decode(img)
 puts decoder.body

 ###
 # Decode a photo from the webcam
 QRTools::UI::Camera.new(0) do |camera|
   puts QRTools::QRCode.decode(camera.capture).body
 end